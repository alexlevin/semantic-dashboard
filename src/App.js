import React, { Component } from 'react';
import TopMenu from './components/TopMenu/TopMenu';
import './App.css';

// Main component in master 4
class App extends Component {
  render() {
    return (
      <div className="App">
        <TopMenu />
      </div>
    );
  }
}

export default App;
